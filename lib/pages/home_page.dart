import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.deepPurple, Colors.purple],
            ),
          ),
          child: Center(
            child: Wrap(
              alignment: WrapAlignment.start,
              direction: Axis.vertical,
              crossAxisAlignment: WrapCrossAlignment.center,
              spacing: 20.0,
              runAlignment: WrapAlignment.spaceBetween,
              children: [
                const SizedBox(
                  height: 500,
                  child: FlutterLogo(size: 50),
                ),

                TextButton(
                  onPressed: () {},
                  style: ButtonStyle(
                    backgroundColor:
                      MaterialStateProperty.all(Colors.white),
                    shape:
                      MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(50))),
                    padding: 
                      MaterialStateProperty.all(const EdgeInsets.only(right: 11, left: 11, top: 5, bottom: 5)),
                    fixedSize: 
                      MaterialStateProperty.all(const Size(300, 30))
                  ),
                  child: const Text('Login With Facebook'),
                ),
                TextButton(
                  onPressed: () {},
                  style: ButtonStyle(
                    backgroundColor:
                      MaterialStateProperty.all(Colors.white),
                    shape:
                      MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(50))),
                    padding: 
                      MaterialStateProperty.all(const EdgeInsets.only(right: 11, left: 11, top: 5, bottom: 5)),
                    fixedSize: 
                      MaterialStateProperty.all(const Size(300, 30))
                  ),
                  child: const Text('Login With Mobile Number'),
                ),
              ],
            ),
          )),
    );
  }
}
